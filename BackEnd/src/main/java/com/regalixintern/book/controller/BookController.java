package com.regalixintern.book.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.regalixintern.book.Book;

import com.regalixintern.book.service.BookService;
import com.regalixintern.controller.exeception.ResourceNotFoundException;

@RestController
@RequestMapping("/books")
@CrossOrigin(origins = {"http://10.250.12.230:4200"})
public class BookController {
	@Autowired
	private BookService bookService;
	
	@RequestMapping("/")
	public List<Book> getAllBooks() {
		return bookService.getAllBooks();
	}
	
	@PostMapping("/")
	public Book addBook( @RequestBody Book book )
	{
		 return bookService.addBook(book);
	}
	
	@RequestMapping("/{id}")
	public Book getBookById(@PathVariable int id) {
		return bookService.getBookById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Book", "id", id));
	}
	
	@PutMapping("/{id}")
	public void updateBook(@PathVariable int id,@RequestBody Book book)
	{
		 bookService.updateBook(book);
	}
	
	@DeleteMapping("/{id}")
	public void deleteBook(@PathVariable int id) {
		 bookService.deleteBook(id);
	}


}
