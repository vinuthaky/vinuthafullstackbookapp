package com.regalixintern.book.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.regalixintern.book.Book;
import com.regalixintern.book.repository.BookRepository;

@Service
public class BookService {
	
	@Autowired
	private BookRepository bookRepository;	
	
	public List<Book> getAllBooks(){		
		return bookRepository.findAll();		
	}
	
	public Optional<Book> getBookById(int id) {
		return bookRepository.findById(id);
	}
	
	public Book addBook(Book book) {
		 return bookRepository.save(book);
	}
	
	public String deleteBook(int id) {
		 bookRepository.deleteById(id);
		 return "Your Book is Successfully deleted";	
	}
	
	public Book updateBook(Book book) {
	 return	bookRepository.save(book);
	}


}
