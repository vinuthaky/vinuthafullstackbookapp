package com.regalixintern.book;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String isbnNo;	
	private String title;	
	private String author;	
	private String description;	
	
	@Override
	public String toString() {
		return "Book [id=" + id + ", isbnNo=" + isbnNo + ", title=" + title + ", author=" + author + ", description="
				+ description + "]";
	}
	public Book() {
		super();
	}
	public Book(int id, String isbnNo, String title, String author, String description) {
		super();
		this.id = id;
		this.isbnNo = isbnNo;
		this.title = title;
		this.author = author;
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIsbnNo() {
		return isbnNo;
	}
	public void setIsbnNo(String isbnNo) {
		this.isbnNo = isbnNo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


}
