import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AddbookComponent } from './component/addbook/addbook.component';
import { ListbooksComponent } from './component/listbooks/listbooks.component';
import { EditbookComponent } from './component/editbook/editbook.component';
import { DetailbookComponent } from './component/detailbook/detailbook.component';
import { HomeComponent } from './component/home/home.component';
import { ErrorComponent } from './component/error/error.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'addBook', component: AddbookComponent},
  {path: 'listBooks', component: ListbooksComponent},
  {path: 'editBook/:id', component: EditbookComponent},
  {path: 'detailBook/:id', component: DetailbookComponent},
  {path: 'error', component: ErrorComponent},
  {path: '**', component: PageNotFoundComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
