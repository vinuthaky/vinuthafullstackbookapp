import { NgModule, ErrorHandler } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import { AddbookComponent } from './component/addbook/addbook.component';
import { ListbooksComponent } from './component/listbooks/listbooks.component';
import { EditbookComponent } from './component/editbook/editbook.component';
import { DetailbookComponent } from './component/detailbook/detailbook.component';
import { HomeComponent } from './component/home/home.component';
import { BooksService } from './service/books.service';
import { ErrorComponent } from './component/error/error.component';
import { GlobalErrorHandler } from './service/global-error-handler.service';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';



@NgModule({
  declarations: [
    AppComponent,
    AddbookComponent,
    ListbooksComponent,
    EditbookComponent,
    DetailbookComponent,
    HomeComponent,
    ErrorComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [BooksService,
               {provide: ErrorHandler, useClass: GlobalErrorHandler}
            ],
  bootstrap: [AppComponent]
})
export class AppModule { }
