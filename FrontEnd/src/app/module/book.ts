export class Book {
    constructor(
        public id: number,
        public isbnNo: string,
        public title: string,
        public author: string,
        public description: string
    ) {}
}
