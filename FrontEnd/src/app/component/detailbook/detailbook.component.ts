import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Book } from 'src/app/module/book';
import { BooksService } from 'src/app/service/books.service';


@Component({
  selector: 'app-detailbook',
  templateUrl: './detailbook.component.html',
  styleUrls: ['./detailbook.component.css']
})
export class DetailbookComponent implements OnInit {

  private book: Book;
  constructor( private bookService: BooksService,
               private router: Router,
               private route: ActivatedRoute) {}

  ngOnInit() {
      this.route.paramMap.subscribe((params: ParamMap) => {
      const  index = parseInt(params.get('id'), 10);
      this.bookService.getBookById(index).subscribe(book => this.book = book);
    });
  }

  edit(book) {
    this.router.navigate(['/editBook', book.id]);
  }

}
