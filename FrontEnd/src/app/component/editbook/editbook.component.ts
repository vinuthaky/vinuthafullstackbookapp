import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Book } from 'src/app/module/book';
import { BooksService } from 'src/app/service/books.service';


@Component({
  selector: 'app-editbook',
  templateUrl: './editbook.component.html',
  styleUrls: ['./editbook.component.css']
})
export class EditbookComponent implements OnInit {

  private book: Book;
  constructor(  private _booksService: BooksService,
                private router: Router,
                private route: ActivatedRoute) { }

  ngOnInit() {
      this.route.paramMap.subscribe((params: ParamMap) => {
      const  index = parseInt(params.get('id'), 10);
      this._booksService.getBookById(index).subscribe(book => this.book = book
        );
    });
  }

 update() {
   this._booksService.editBook(this.book).subscribe(() =>
   this.router.navigate(['/listBooks']));
  }

}
