package com.regalixintern.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.regalixintern.book.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

}
